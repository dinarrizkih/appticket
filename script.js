function hitungtotal() {
    var tujuan = document.getElementById('destination').value;
    var jumlahtiket = parseFloat(document.getElementById('ticketQuantity').value);
    var ht = 0;
    var sub = 0;
    var diskon = 0;
    var total = 0;

    if (tujuan == "Jakarta") {
        ht = 70000;
    } else if (tujuan == "Solo") {
        ht = 80000;
    } else if (tujuan == "Surabaya") {
        ht = 90000;
    } else if (tujuan == "Bali") {
        ht = 120000;
    } else if (tujuan == "Yogyakarta") {
        ht = 100000;
    } else if (tujuan == "Medan") {
        ht = 150000;
    } else if (tujuan == "Bandung") {
        ht = 75000;
    } else if (tujuan == "Palembang") {
        ht = 110000;
    } else if (tujuan == "Makassar") {
        ht = 130000;
    }

    sub = jumlahtiket * ht;

    if (document.getElementById('member').checked) {
        diskon = 0.10 * sub;
    } else {
        diskon = 0;
    }

    total = sub - diskon;

    document.getElementById('ticketPrice').value = ht;
    document.getElementById('ticketPrice').value = ht * jumlahtiket;
    document.getElementById('discount').value = diskon;
    document.getElementById('totalPayment').value = total;
}
